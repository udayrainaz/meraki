# meraki

MERAKI : This is a word that modern Greeks often use to describe what happens when you leave a piece of yourself (your soul, creativity, or love) in your work. When you love doing something, anything, so much that you put something of yours.